package com.chatt.ui;
import java.util.ArrayList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.chatt.R;
import com.chatt.custom.CustomFragment;
import com.chatt.model.ChatItem;

/**
 * The Class ChatList is the Fragment class that is launched when the user
 * clicks on Chats option in Left navigation drawer. It shows a dummy list of
 * user's chats. You need to write your own code to load and display actual
 * chat.
 */
public class SalonDetay extends CustomFragment
{

    /** The Chat list. */
    private ArrayList<ChatItem> chatList=new ArrayList<ChatItem>();
    public ChatAdapter ca;
    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.salon_detay, null);

        ca = new ChatAdapter();
        ListView list = (ListView) v.findViewById(R.id.list);
        list.setAdapter(ca);
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3)
            {
            }
        });
        loadSalonOzellikleri();
		setTouchNClick(v.findViewById(R.id.tabSalonDetaySol));
		setTouchNClick(v.findViewById(R.id.tabSalonDetayOrta));
		setTouchNClick(v.findViewById(R.id.tabSalonDetaySag));
        return v;
    }

    /* (non-Javadoc)
     * @see com.socialshare.custom.CustomFragment#onClick(android.view.View)
     */
    @Override
    public void onClick(View v)
    {
        super.onClick(v);
        if (v.getId() == R.id.tabSalonDetaySol)
        {
            loadSalonOzellikleri();


        }
        else if (v.getId() == R.id.tabSalonDetayOrta)
        {
            //load kişiler

        }
        else if (v.getId() == R.id.tabSalonDetaySag) {
            //kişiyi salona ekle
        }
    }

    /**
     * This method currently loads a dummy list of chats. You can write the
     * actual implementation of loading chats.
     */
    private void loadSalonOzellikleri()
    {
        ArrayList<ChatItem> chatList = new ArrayList<ChatItem>();

        chatList.add(new ChatItem("10.00-22.00", "",
                "Saatleri Arası Salonumuzu Kullanabilirsiniz","", R.drawable.clock_salon, true,
                false));
        chatList.add(new ChatItem("Sauna Havuz Antrenör", "",
                "Salonumuzun Sunduğu Hizmetler Arasındadır","", R.drawable.dumbbell_salon, true,
                false));
        this.chatList=new ArrayList<ChatItem>(chatList);

        ca.notifyDataSetChanged();
    }
    private void loadChatList()
    {
        ArrayList<ChatItem> chatList = new ArrayList<ChatItem>();

        chatList.add(new ChatItem(
                "Jonathan L, Matt G, Neha D.",
                "Friends",
                "I agree with you and with "
                        + "your thoughts, you are right! We need some changes in life",
                "08:20AM", R.drawable.group1, true, true));
        chatList.add(new ChatItem("Victor Holdings", "Urgent!",
                "Hey man, long time. Hows your work goin"
                        + " on and hows your life?", "01/20/2014",
                R.drawable.user2, false, false));
        chatList.add(new ChatItem("Linda Hines", "Vacations...",
                "How are you?", "01/20/2014", R.drawable.user3, false, false));
        chatList.add(new ChatItem("Nelsy thomas", "Life!",
                "Hope you and your team is doing great. I know you are"
                        + " very busy with work", "01/22/2014",
                R.drawable.user4, true, false));
        chatList.add(new ChatItem("John E, Matt G., Victor D", "Happy hours",
                "Yes, it's very good", "01/22/2014", R.drawable.group2, true,
                true));
        chatList.add(new ChatItem("Martin K", "Last party",
                "This is last party...", "01/22/2014", R.drawable.user5, false,
                false));
        this.chatList = new ArrayList<ChatItem>(chatList);
        this.chatList.addAll(chatList);
        this.chatList.addAll(chatList);

    }

    /**
     * The Class CutsomAdapter is the adapter class for Chat ListView. The
     * currently implementation of this adapter simply display static dummy
     * contents. You need to write the code for displaying actual contents.
     */
    private class ChatAdapter extends BaseAdapter
    {

        /* (non-Javadoc)
         * @see android.widget.Adapter#getCount()
         */
        @Override
        public int getCount()
        {
            return chatList.size();
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getItem(int)
         */
        @Override
        public ChatItem getItem(int arg0)
        {
            return chatList.get(arg0);
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getItemId(int)
         */
        @Override
        public long getItemId(int arg0)
        {
            return arg0;
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
         */
        @Override
        public View getView(int pos, View v, ViewGroup arg2)
        {
            if (v == null)
                v = LayoutInflater.from(getActivity()).inflate(
                        R.layout.chat_item, null);

            ChatItem c = getItem(pos);
            TextView lbl = (TextView) v.findViewById(R.id.lbl1);
            lbl.setText(c.getName());

            lbl = (TextView) v.findViewById(R.id.lbl2);
            lbl.setText(c.getDate());

            lbl = (TextView) v.findViewById(R.id.lbl3);
            lbl.setText(c.getTitle());

            lbl = (TextView) v.findViewById(R.id.lbl4);
            lbl.setText(c.getMsg());

            ImageView img = (ImageView) v.findViewById(R.id.img1);
            img.setImageResource(c.getIcon());

            img = (ImageView) v.findViewById(R.id.img2);
            img.setImageResource(c.isGroup() ? R.drawable.ic_group
                    : R.drawable.ic_lock);

            img = (ImageView) v.findViewById(R.id.online);
            img.setVisibility(c.isOnline() ? View.VISIBLE : View.INVISIBLE);
            return v;
        }

    }
}
