package com.chatt;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.chatt.custom.CustomActivity;
import com.chatt.dao.Kisi;
import com.chatt.helper.KisiHelper;

/**
 * The Class Login is an Activity class that shows the login screen to users.
 * The current implementation simply start the MainActivity. You can write your
 * own logic for actual login and for login using Facebook and Twitter.
 */
public class KayitOl2 extends CustomActivity
{
    public int kisiID;
    public Kisi guncellenecekKisi;
    public KisiHelper kisiHelper;
    EditText Boy;
    EditText Kilo;
    EditText DogumTarihi;
    Spinner Cinsiyet;

    /* (non-Javadoc)
     * @see com.chatt.custom.CustomActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kayit_ol2);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            kisiID = extras.getInt("kisiID");
        }
        kisiHelper = new KisiHelper(getBaseContext());
        guncellenecekKisi= kisiHelper.getKisi(kisiID);
        Boy= (EditText)findViewById(R.id.txtBoy);
        Kilo= (EditText)findViewById(R.id.txtKilo);
        DogumTarihi= (EditText)findViewById(R.id.txtDogumTarihi);
        Cinsiyet = (Spinner) findViewById(R.id.spinnerCinsiyet);
        setTouchNClick(R.id.btnRegister2);
    }

    /* (non-Javadoc)
     * @see com.chatt.custom.CustomActivity#onClick(android.view.View)
     */
    @Override
    public void onClick(View v)
    {
        super.onClick(v);
        if (v.getId() == R.id.btnRegister2)
        {
            guncellenecekKisi.setBoy(Integer.parseInt(Boy.getText().toString()));
            guncellenecekKisi.setKilo(Integer.parseInt(Kilo.getText().toString()));
            guncellenecekKisi.setDogumTarihi(DogumTarihi.getText() + "");
            guncellenecekKisi.setCinsiyet(Cinsiyet.getSelectedItem().toString());
            kisiHelper.creatOrUpdateKisi(guncellenecekKisi);
            Intent intent= new Intent(this, Olcu.class);
            Log.d("kişinin id si", guncellenecekKisi.getId() + "");
            intent.putExtra("kisiID",guncellenecekKisi.getId());
            startActivity(intent);
            finish();

        }
    }
}
