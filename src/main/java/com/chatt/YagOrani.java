package com.chatt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.chatt.custom.CustomActivity;
import com.chatt.dao.Kisi;
import com.chatt.helper.KisiHelper;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Login is an Activity class that shows the login screen to users.
 * The current implementation simply start the MainActivity. You can write your
 * own logic for actual login and for login using Facebook and Twitter.
 */
public class YagOrani extends CustomActivity
{


    public int kisiID;
    public Kisi guncellenecekKisi;
    public KisiHelper kisiHelper;
    SliderLayout sliderShow=null;
    int lavukYuzde=4;
    int kadinYuzde = 7;
    /* (non-Javadoc)
     * @see com.chatt.custom.CustomActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yag_orani);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            kisiID = extras.getInt("kisiID");
        }
        kisiHelper = new KisiHelper(getBaseContext());
        guncellenecekKisi= kisiHelper.getKisi(kisiID);
        sliderShow = (SliderLayout) findViewById(R.id.slider);

        List<Integer> LavukImageHolder = new ArrayList<Integer>();
        LavukImageHolder.add(R.drawable.e_vucut_1);
        LavukImageHolder.add(R.drawable.e_vucut_2);
        LavukImageHolder.add(R.drawable.e_vucut_3);
        LavukImageHolder.add(R.drawable.e_vucut_4);
        LavukImageHolder.add(R.drawable.e_vucut_5);
        LavukImageHolder.add(R.drawable.e_vucut_6);
        LavukImageHolder.add(R.drawable.e_vucut_7);
        LavukImageHolder.add(R.drawable.e_vucut_8);
        LavukImageHolder.add(R.drawable.e_vucut_9);
        List<Integer> KadinImageHolder = new ArrayList<Integer>();
        KadinImageHolder.add(R.drawable.k_vucut_1);
        KadinImageHolder.add(R.drawable.k_vucut_2);
        KadinImageHolder.add(R.drawable.k_vucut_3);
        KadinImageHolder.add(R.drawable.k_vucut_4);
        KadinImageHolder.add(R.drawable.k_vucut_5);
        KadinImageHolder.add(R.drawable.k_vucut_6);
        KadinImageHolder.add(R.drawable.k_vucut_7);
        KadinImageHolder.add(R.drawable.k_vucut_8);
        KadinImageHolder.add(R.drawable.k_vucut_9);

        sliderShow.stopAutoCycle();
        for(int i=0;i<9;i++)
        {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .description("%"+lavukYuzde)
                    .image(LavukImageHolder.get(i));
            textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(final BaseSliderView baseSliderView) {


                    new AlertDialog.Builder(YagOrani.this)
                            .setTitle("Yağ Oranınız")
                            .setMessage(baseSliderView.getDescription() + " Olduğuna Emin misiniz ? ")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                     String aciklama=  baseSliderView.getDescription();
                                    aciklama=aciklama.replace("%","");
                                    int yuzde = Integer.parseInt(aciklama);
                                    guncellenecekKisi.setYagOrani(yuzde);
                                    kisiHelper.creatOrUpdateKisi(guncellenecekKisi);
                                    Intent intent= new Intent(YagOrani.this, MainActivity.class);
                                    Log.d("kişinin id si", guncellenecekKisi.getId() + "");
                                    intent.putExtra("kisiID",guncellenecekKisi.getId());
                                    Log.d("kişi bilgisi",guncellenecekKisi.toString());
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            });

            sliderShow.addSlider(textSliderView);
            lavukYuzde+=5;
        }

    }

    @Override
    protected void onStop() {
        sliderShow.stopAutoCycle();
        super.onStop();
    }

    /* (non-Javadoc)
     * @see com.chatt.custom.CustomActivity#onClick(android.view.View)
     */
    @Override
    public void onClick(View v)
    {
        super.onClick(v);
        if (v.getId() == R.id.btnOlcu)
        {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
}
