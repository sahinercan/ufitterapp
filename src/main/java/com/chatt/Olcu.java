package com.chatt;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.chatt.custom.CustomActivity;
import com.chatt.dao.Kisi;
import com.chatt.helper.KisiHelper;

/**
 * The Class Login is an Activity class that shows the login screen to users.
 * The current implementation simply start the MainActivity. You can write your
 * own logic for actual login and for login using Facebook and Twitter.
 */
public class Olcu extends CustomActivity
{

    public int kisiID;
    public Kisi guncellenecekKisi;
    public KisiHelper kisiHelper;

    EditText Omuz;
    EditText Gogus;
    EditText Bel;
    EditText Baldir;
    EditText UstKol;
    EditText OnKol;
    EditText Kalf;

    /* (non-Javadoc)
     * @see com.chatt.custom.CustomActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.olcu);
        setTouchNClick(R.id.btnOlcu);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            kisiID = extras.getInt("kisiID");
        }
        kisiHelper = new KisiHelper(getBaseContext());
        guncellenecekKisi= kisiHelper.getKisi(kisiID);
        Omuz= (EditText)findViewById(R.id.txtOmuz);
        Gogus= (EditText)findViewById(R.id.txtGogus);
        Bel= (EditText)findViewById(R.id.txtBel);
        Baldir= (EditText)findViewById(R.id.txtBaldir);
        UstKol= (EditText)findViewById(R.id.txtUstKol);
        OnKol= (EditText)findViewById(R.id.txtOnKol);
        Kalf= (EditText)findViewById(R.id.txtKalf);

    }

    /* (non-Javadoc)
     * @see com.chatt.custom.CustomActivity#onClick(android.view.View)
     */
    @Override
    public void onClick(View v)
    {
        super.onClick(v);
        if (v.getId() == R.id.btnOlcu)
        {
            guncellenecekKisi.setOmuz(Integer.parseInt(Omuz.getText().toString()));
            guncellenecekKisi.setGogus(Integer.parseInt(Gogus.getText().toString()));
            guncellenecekKisi.setBel(Integer.parseInt(Bel.getText().toString()));
            guncellenecekKisi.setBaldir(Integer.parseInt(Baldir.getText().toString()));
            guncellenecekKisi.setUstKol(Integer.parseInt(UstKol.getText().toString()));
            guncellenecekKisi.setOnKol(Integer.parseInt(OnKol.getText().toString()));
            guncellenecekKisi.setKalf(Integer.parseInt(Kalf.getText().toString()));
            kisiHelper.creatOrUpdateKisi(guncellenecekKisi);
            Intent intent= new Intent(this, YagOrani.class);
            Log.d("kişinin id si", guncellenecekKisi.getId() + "");
            intent.putExtra("kisiID",guncellenecekKisi.getId());
            startActivity(intent);
            finish();

        }
    }
}
