package com.chatt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.chatt.custom.CustomActivity;
import com.chatt.dao.Kisi;
import com.chatt.helper.KisiHelper;

/**
 * The Class Login is an Activity class that shows the login screen to users.
 * The current implementation simply start the MainActivity. You can write your
 * own logic for actual login and for login using Facebook and Twitter.
 */
public class KayitOl extends CustomActivity
{

    EditText Ad;
    EditText Soyad;
    EditText Mail;
    EditText Sifre;
    Spinner Il;
    Spinner Ilce;
    /* (non-Javadoc)
     * @see com.chatt.custom.CustomActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kayit_ol);
        setTouchNClick(R.id.btnRegister);
        Ad= (EditText)findViewById(R.id.txtAd);
        Soyad= (EditText)findViewById(R.id.txtSoyad);
        Mail= (EditText)findViewById(R.id.txtMail);
        Sifre = (EditText)findViewById(R.id.txtSifre);
        Il = (Spinner) findViewById(R.id.spinnerIl);
        Ilce = (Spinner) findViewById(R.id.spinnerIlce);

    }

    /* (non-Javadoc)
     * @see com.chatt.custom.CustomActivity#onClick(android.view.View)
     */
    @Override
    public void onClick(View v)
    {
        super.onClick(v);
        if (v.getId() == R.id.btnRegister)
        {
            final KisiHelper kisiHelper = new KisiHelper(getBaseContext());
            Kisi kisi = new Kisi();
            kisi.setAd(Ad.getText()+"");
            kisi.setSoyad(Soyad.getText() + "");
            kisi.setMail(Mail.getText() + "");
            kisi.setSifre(Sifre.getText() + "");
            kisi.setIl(Il.getSelectedItem().toString());
            kisi.setIlce(Ilce.getSelectedItem().toString());
            kisiHelper.creatOrUpdateKisi(kisi);
            Intent intent= new Intent(this, KayitOl2.class);
            Log.d("kişinin id si",kisi.getId()+"");
            intent.putExtra("kisiID",kisi.getId());
            startActivity(intent);
            finish();
        }
    }
}
