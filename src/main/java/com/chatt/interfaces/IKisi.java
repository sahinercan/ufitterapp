package com.chatt.interfaces;

import com.chatt.dao.Kisi;

import java.util.List;

/**
 * Created on 10/06/15.
 */
public interface IKisi {

    public Kisi addKisi(Kisi kisi);
    public void deleteKisi(Kisi kisi);
    public void deleteAllKisi();
    public Kisi getKisi(int id);
    public List<Kisi> getAllKisi();
    public Kisi creatOrUpdateKisi(Kisi kisi);
    public Kisi kisiSifreDogrula(String mail, String sifre);
}
