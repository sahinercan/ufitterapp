package com.chatt.helper;

import android.content.Context;

import com.chatt.dao.Kisi;
import com.chatt.interfaces.IKisi;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

/**
 * Created on 10/06/15.
 */
public class KisiHelper implements IKisi {

    private Context context;
    private DatabaseHelper dbHelper;

    public KisiHelper(Context context) {
        this.context = context;

    }
    @Override
    public Kisi addKisi(Kisi kisi) {
        this.dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        RuntimeExceptionDao<Kisi, Integer> kisiDao = dbHelper.getRuntimeExceptionDao(Kisi.class);
        kisiDao.create(kisi);
        OpenHelperManager.releaseHelper();
        return kisi;
    }

    @Override
    public void deleteKisi(Kisi kisi) {
        this.dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        RuntimeExceptionDao<Kisi, Integer> kisiDao = dbHelper.getRuntimeExceptionDao(Kisi.class);
        kisiDao.delete(kisi);
        OpenHelperManager.releaseHelper();
    }

    @Override
    public void deleteAllKisi() {
        this.dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        RuntimeExceptionDao<Kisi, Integer> kisiDao = dbHelper.getRuntimeExceptionDao(Kisi.class);
        kisiDao.delete(kisiDao.queryForAll());
        OpenHelperManager.releaseHelper();
    }


    @Override
    public Kisi getKisi(int id) {
        this.dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        RuntimeExceptionDao<Kisi, Integer> profileDao = dbHelper.getRuntimeExceptionDao(Kisi.class);
        List<Kisi> cocuklar = profileDao.queryForEq("id", id);
        OpenHelperManager.releaseHelper();
        if (cocuklar.size() > 0) {
            return cocuklar.get(0);
        }else{
            return null;
        }
    }

    @Override
    public List<Kisi> getAllKisi() {
        this.dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        RuntimeExceptionDao<Kisi, Integer> profileDao = dbHelper.getRuntimeExceptionDao(Kisi.class);

        List<Kisi> kisiler = null;

        try {
            kisiler = profileDao.queryBuilder().orderBy("id",true).query();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        OpenHelperManager.releaseHelper();

        return kisiler;
    }

    @Override
    public Kisi kisiSifreDogrula(String mail, String sifre)
    {
        this.dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        RuntimeExceptionDao<Kisi, Integer> profileDao = dbHelper.getRuntimeExceptionDao(Kisi.class);

        List<Kisi> kisiler = null;

        try {
            kisiler = profileDao.queryBuilder().where().eq("Mail",mail).and().eq("Sifre",sifre).query();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        OpenHelperManager.releaseHelper();

        return kisiler.get(0);

    }
    @Override
    public Kisi creatOrUpdateKisi(Kisi kisi) {
        this.dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        RuntimeExceptionDao<Kisi, Integer> kisiDao = dbHelper.getRuntimeExceptionDao(Kisi.class);

        List<Kisi> profiles = null;


        profiles = kisiDao.queryForEq("id", kisi.getId());


        if (profiles.size() > 0) {
            Kisi dbProfile = profiles.get(0);
            kisiDao.update(kisi);
        }else{
            kisiDao.create(kisi);
        }
        OpenHelperManager.releaseHelper();
        return kisi;
    }
}
