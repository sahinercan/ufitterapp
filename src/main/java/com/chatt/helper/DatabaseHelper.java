package com.chatt.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.chatt.dao.Kisi;
import com.chatt.dao.SalonKisiler;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created on 11.10.14.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "ufitter.db";
    private static final int DATABASE_VERSION = 4;

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void clear(Class myClass) throws SQLException {
        TableUtils.clearTable(connectionSource, myClass);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Kisi.class);
            TableUtils.createTableIfNotExists(connectionSource, SalonKisiler.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource,Kisi.class,true);
            TableUtils.dropTable(connectionSource,SalonKisiler.class,true);
            onCreate(database,connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
