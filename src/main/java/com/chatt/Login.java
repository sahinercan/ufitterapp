package com.chatt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.chatt.custom.CustomActivity;
import com.chatt.dao.Kisi;
import com.chatt.helper.KisiHelper;

/**
 * The Class Login is an Activity class that shows the login screen to users.
 * The current implementation simply start the MainActivity. You can write your
 * own logic for actual login and for login using Facebook and Twitter.
 */
public class Login extends CustomActivity
{

    EditText mail;
    EditText sifre;
	/* (non-Javadoc)
	 * @see com.chatt.custom.CustomActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
        mail = (EditText) findViewById(R.id.txtUsername);
        sifre = (EditText) findViewById(R.id.txtPassword);
		setTouchNClick(R.id.btnGiris);
		setTouchNClick(R.id.btnKayitOl);
	}

	/* (non-Javadoc)
	 * @see com.chatt.custom.CustomActivity#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v)
	{
		super.onClick(v);
		if (v.getId() == R.id.btnKayitOl)
		{
			startActivity(new Intent(this, KayitOl.class));
			finish();
		}

        else if (v.getId() == R.id.btnGiris)
        {
            KisiHelper kisiHelper = new KisiHelper(getBaseContext());
            Kisi obj  = kisiHelper.kisiSifreDogrula(mail.getText()+"",sifre.getText()+"");
            if(obj!=null)
            {
                Globals.sessionKisi=obj;
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
           else
            {
                CharSequence text = "Kullanıcı Adı Veya Şifre Yanlış!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                toast.show();
            }

        }
	}
}
