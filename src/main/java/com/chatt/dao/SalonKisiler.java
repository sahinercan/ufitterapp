package com.chatt.dao;

import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

/**
 * Created on 10/06/15.
 */
public class SalonKisiler {

    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField
    String adi;
    @DatabaseField
    Date dogumTarihi;
    @DatabaseField
    String imagePath;
}
