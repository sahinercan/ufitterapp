package com.chatt.dao;

import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

/**
 * Created on 10/06/15.
 */
public class Kisi {

    public Kisi()
    {
        setAd("");
        setSoyad("");
        setKalf(0);
        setMail("");
        setIl("");
        setOnKol(0);
        setBaldir(0);
        setBel(0);
        setBoy(0);
        setCinsiyet("");
        setDogumTarihi("");
        setGogus(0);
        setIlce("");
        setOmuz(0);
        setYagOrani(0);
        setUstKol(0);
        setSifre("");
        setKilo(0);
    }
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField
    String Ad;
    @DatabaseField
    String Soyad;
    @DatabaseField
    String Mail;
    @DatabaseField
    String Sifre;
    @DatabaseField
    String Il;
    @DatabaseField
    String Ilce;
    @DatabaseField
    Integer Boy;
    @DatabaseField
    Integer Kilo;
    @DatabaseField
    String DogumTarihi;
    @DatabaseField
    String Cinsiyet;
    @DatabaseField
    Integer Omuz;
    @DatabaseField
    Integer Gogus;
    @DatabaseField
    Integer Bel;
    @DatabaseField
    Integer Baldir;
    @DatabaseField
    Integer UstKol;
    @DatabaseField
    Integer OnKol;
    @DatabaseField
    Integer Kalf;
    @DatabaseField
    Integer YagOrani;

    public Integer getOmuz() {
        return Omuz;
    }

    public Integer getYagOrani() {
        return YagOrani;
    }

    public void setYagOrani(Integer yagOrani) {
        YagOrani = yagOrani;
    }

    public void setOmuz(Integer omuz) {
        Omuz = omuz;
    }

    public Integer getGogus() {
        return Gogus;
    }

    public void setGogus(Integer gogus) {
        Gogus = gogus;
    }

    public Integer getBel() {
        return Bel;
    }

    public void setBel(Integer bel) {
        Bel = bel;
    }

    public Integer getBaldir() {
        return Baldir;
    }

    public void setBaldir(Integer baldir) {
        Baldir = baldir;
    }

    public Integer getUstKol() {
        return UstKol;
    }

    public void setUstKol(Integer ustKol) {
        UstKol = ustKol;
    }

    public Integer getOnKol() {
        return OnKol;
    }

    public void setOnKol(Integer onKol) {
        OnKol = onKol;
    }

    public Integer getKalf() {
        return Kalf;
    }

    public void setKalf(Integer kalf) {
        Kalf = kalf;
    }

    public Integer getBoy() {
        return Boy;
    }

    public void setBoy(Integer boy) {
        Boy = boy;
    }

    public Integer getKilo() {
        return Kilo;
    }

    public void setKilo(Integer kilo) {
        Kilo = kilo;
    }

    public String getCinsiyet() {
        return Cinsiyet;
    }

    public void setCinsiyet(String cinsiyet) {
        Cinsiyet = cinsiyet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAd() {
        return Ad;
    }

    public void setAd(String ad) {
        Ad = ad;
    }

    public String getSoyad() {
        return Soyad;
    }

    public void setSoyad(String soyad) {
        Soyad = soyad;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getSifre() {
        return Sifre;
    }

    public void setSifre(String sifre) {
        Sifre = sifre;
    }

    public String getIl() {
        return Il;
    }

    public void setIl(String il) {
        Il = il;
    }

    public String getIlce() {
        return Ilce;
    }

    public void setIlce(String ilce) {
        Ilce = ilce;
    }

    public String getDogumTarihi() {
        return DogumTarihi;
    }

    public void setDogumTarihi(String dogumTarihi) {
        DogumTarihi = dogumTarihi;
    }
}
